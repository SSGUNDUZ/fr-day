package com.example.demo.service

import com.example.demo.constants.FullAddresses
import com.example.demo.constants.FullAddresses.Companion.AM_BACHLE
import com.example.demo.constants.FullAddresses.Companion.AUF_DER_VOGELWIESE_ADDRESS
import com.example.demo.constants.FullAddresses.Companion.BLAUFELWEG_123B
import com.example.demo.constants.FullAddresses.Companion.BROADWAY_AV_ADDRESS
import com.example.demo.constants.FullAddresses.Companion.CALLE_39_ADDRESS
import com.example.demo.exceptions.EmptyAddressException
import com.example.demo.exceptions.InterPreterNotFoundException
import com.example.demo.model.AddressDTO
import com.example.demo.repository.AddressRepository
import org.assertj.core.api.Assertions.assertThat
import org.assertj.core.api.ThrowableAssert.catchThrowable
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.BDDMockito.given
import org.mockito.Mockito.verify
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest
import org.springframework.boot.test.mock.mockito.MockBean
import org.springframework.test.context.junit4.SpringRunner

@RunWith(SpringRunner::class)
@WebMvcTest(AddressService::class)
class AddressServiceTest {

    @Autowired
    private lateinit var address: AddressService

    @MockBean
    private lateinit var addressRepository: AddressRepository


    @Test
    fun should_interpret_an_address_with_address_string_and_number() {

        val splitAddress = address.interpretAddress(CALLE_39_ADDRESS)
        val addressDTO = AddressDTO(splitAddress.Street, splitAddress.Number)
        given(addressRepository.save(addressDTO)).willReturn(addressDTO)

        assertThat(addressDTO.Street).isEqualTo("Calle 39")
        assertThat(addressDTO.Number).isEqualTo("No 1540")
        verify(addressRepository).save(addressDTO)

    }

    @Test
    fun should_interpret_an_address_with_address_number_first_street_later_includes_special_character() {

        val splitAddress = address.interpretAddress(FullAddresses.RUE_DE_LA_REVOLUT)
        val addressDTO = AddressDTO(splitAddress.Street, splitAddress.Number)
        given(addressRepository.save(addressDTO)).willReturn(addressDTO)

        assertThat(addressDTO.Street).isEqualTo("rue de la revolution")
        assertThat(addressDTO.Number).isEqualTo("4")
        verify(addressRepository).save(addressDTO)

    }

    @Test
    fun should_interpret_an_address_with_address_street_first_number_later_includes_special_character() {

        val splitAddress = address.interpretAddress(FullAddresses.CALLE_ADUANA)
        val addressDTO = AddressDTO(splitAddress.Street, splitAddress.Number)
        given(addressRepository.save(addressDTO)).willReturn(addressDTO)


        assertThat(addressDTO.Street).isEqualTo("Calle Aduana")
        assertThat(addressDTO.Number).isEqualTo("29")
        verify(addressRepository).save(addressDTO)

    }

    @Test
    fun should_interpret_an_address_with_number_includes_letters() {

        val splitAddress = address.interpretAddress(BLAUFELWEG_123B)
        val addressDTO = AddressDTO(splitAddress.Street, splitAddress.Number)
        given(addressRepository.save(addressDTO)).willReturn(addressDTO)


        assertThat(addressDTO.Street).isEqualTo("Blaufeldweg")
        assertThat(addressDTO.Number).isEqualTo("123B")
        verify(addressRepository).save(addressDTO)

    }

    @Test
    fun should_interpret_addresses_with_special_letters() {

        val splitAddress = address.interpretAddress(AM_BACHLE)
        val addressDTO = AddressDTO(splitAddress.Street, splitAddress.Number)
        given(addressRepository.save(addressDTO)).willReturn(addressDTO)


        assertThat(addressDTO.Street).isEqualTo("Am Bächle")
        assertThat(addressDTO.Number).isEqualTo("23")
        verify(addressRepository).save(addressDTO)

    }

    @Test
    fun should_interpret_an_number_includes_letter_and_space() {

        val splitAddress = address.interpretAddress(AUF_DER_VOGELWIESE_ADDRESS)
        val addressDTO = AddressDTO(splitAddress.Street, splitAddress.Number)
        given(addressRepository.save(addressDTO)).willReturn(addressDTO)

        assertThat(addressDTO.Street).isEqualTo("Auf der Vogelwiese")
        assertThat(addressDTO.Number).isEqualTo("23 b")
        verify(addressRepository).save(addressDTO)

    }

    @Test
    fun should_parse_an_international_address_() {

        val splitAddress = address.interpretAddress(BROADWAY_AV_ADDRESS)
        val addressDTO = AddressDTO(splitAddress.Street, splitAddress.Number)
        given(addressRepository.save(addressDTO)).willReturn(addressDTO)


        assertThat(addressDTO.Street).isEqualTo("Broadway Av")
        assertThat(addressDTO.Number).isEqualTo("200")
        verify(addressRepository).save(addressDTO)
    }

    @Test
    fun when_address_is_null_or_empty_should_throw_EmptyAddressException() {

        val thrown = catchThrowable { address.interpretAddress("") }
        assertThat(thrown).isExactlyInstanceOf(EmptyAddressException::class.java)
        assertThat(thrown).hasMessageContaining("Empty address cannot be interpreted")
    }

    @Test
    fun when_address_is_null_or_empty_should_throw_InterPreterNotFoundException() {

        val thrown = catchThrowable { address.interpretAddress("{}{}{}{") }
        assertThat(thrown).isExactlyInstanceOf(InterPreterNotFoundException::class.java)
        assertThat(thrown).hasMessageContaining("There is no interpreter for given address")
    }

}
