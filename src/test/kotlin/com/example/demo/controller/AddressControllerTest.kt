package com.example.demo.controller

import com.example.demo.model.AddressDTO
import com.example.demo.service.AddressService
import com.fasterxml.jackson.module.kotlin.jacksonObjectMapper
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.BDDMockito
import org.mockito.Mockito.times
import org.mockito.Mockito.verify
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.boot.test.mock.mockito.MockBean
import org.springframework.http.MediaType
import org.springframework.test.context.junit4.SpringRunner
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders
import org.springframework.test.web.servlet.result.MockMvcResultMatchers

@RunWith(SpringRunner::class)
@SpringBootTest
@AutoConfigureMockMvc
class AddressControllerTest {

    @Autowired
    private lateinit var mockMvc: MockMvc

    @MockBean
    lateinit var addressService: AddressService

    private val mapper = jacksonObjectMapper()

    private lateinit var addressString: String

    private lateinit var address: String

    private lateinit var addressDTO: AddressDTO


    @Test
    fun `make a POST call split address`() {
        address = "Auf der Vogelwiese 23 b"
        addressDTO = AddressDTO("Auf der Vogelwiese", "23 b")
        addressString = mapper.writeValueAsString(addressDTO)

        BDDMockito.given(addressService.interpretAddress(address)).willReturn(addressDTO)

        mockMvc.perform(
                MockMvcRequestBuilders.post("/address")
                        .contentType(MediaType.TEXT_PLAIN)
                        .content("Auf der Vogelwiese 23 b"))
                .andExpect(MockMvcResultMatchers.content().string(addressString))
                .andExpect(MockMvcResultMatchers.status().isOk)

        verify(addressService, times(1)).interpretAddress(address)
    }


}
