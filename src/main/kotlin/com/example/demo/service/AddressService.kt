package com.example.demo.service

import com.example.demo.constants.Regex.Companion.LATINIC_ALPHABET
import com.example.demo.constants.Regex.Companion.NUMBER_INCLUDING_ADDRESS
import com.example.demo.constants.Regex.Companion.SPECIAL_LETTERS_ALPHABET
import com.example.demo.exceptions.EmptyAddressException
import com.example.demo.exceptions.InterPreterNotFoundException
import com.example.demo.model.AddressDTO
import com.example.demo.repository.AddressRepository
import com.example.demo.utils.AddressCleanup.removeComma
import com.example.demo.utils.AddressMatch
import com.example.demo.utils.AddressMatch.getAddressMatch
import org.apache.commons.lang.math.NumberUtils
import org.slf4j.LoggerFactory
import org.springframework.stereotype.Service

@Service
class AddressService(private val addressRepository: AddressRepository) {

    private var interpretedAddress = arrayOfNulls<String>(2)

    private val logger = LoggerFactory.getLogger(AddressService::class.java)

    @Throws(InterPreterNotFoundException::class)
    fun interpretAddress(address: String): AddressDTO {

        logger.info(" The AddressService has been started to interpret address")
        if (address.isEmpty()) {
            throw EmptyAddressException()
        }

        val cleanedAddress = removeComma(address)

        var addressMatcher = AddressMatch.getAddressMatch(NUMBER_INCLUDING_ADDRESS, cleanedAddress)

        if (addressMatcher.find()) {

            val digitInAddress = getDigitsFromAddress(cleanedAddress)

            val numberOfTheStreet = digitInAddress[0]

            val numberStartPos = cleanedAddress.indexOf(numberOfTheStreet) + numberOfTheStreet.length

            val street = cleanedAddress.substring(0, numberStartPos)

            val number = cleanedAddress.substring(numberStartPos)

            val addressDTO = AddressDTO(street.trim(), number.trim())

            addressRepository.save(addressDTO)

            return addressDTO
        }

        addressMatcher = getAddressMatch(SPECIAL_LETTERS_ALPHABET, cleanedAddress)

        if (addressMatcher.find()) {
            for (i in 1..addressMatcher.groupCount()) {
                interpretedAddress[i - 1] = addressMatcher.group(i).trim()
            }

            val addressDTO = AddressDTO(interpretedAddress[0]!!, interpretedAddress[1]!!)

            addressRepository.save(addressDTO)

            return addressDTO
        }

        addressMatcher = getAddressMatch(LATINIC_ALPHABET, cleanedAddress)

        if (addressMatcher.find()) {
            for (i in 1..addressMatcher.groupCount()) {
                val data = addressMatcher.group(i).trim()
                if (NumberUtils.isNumber(data)) {
                    interpretedAddress[1] = data
                }
                interpretedAddress[0] = data
            }

            val addressDTO = AddressDTO(interpretedAddress[0]!!, interpretedAddress[1]!!)

            addressRepository.save(addressDTO)

            return addressDTO
        }

        throw InterPreterNotFoundException(cleanedAddress)
    }


    private fun getDigitsFromAddress(fullAddress: String): Array<String> {
        return fullAddress
                .replace("[a-zA-Záéíóúä]".toRegex(), "")
                .trim { it <= ' ' }
                .split(" ".toRegex())
                .dropLastWhile { it.isEmpty() }.toTypedArray();
    }
}
