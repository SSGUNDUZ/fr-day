package com.example.demo.repository

import com.example.demo.model.AddressDTO
import org.springframework.data.mongodb.repository.MongoRepository
import org.springframework.stereotype.Repository

@Repository
interface AddressRepository : MongoRepository<AddressDTO, String>
