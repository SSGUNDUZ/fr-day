package com.example.demo.utils

import com.example.demo.constants.Regex

object AddressCleanup {

    fun removeComma(address: String): String {
        return address.replace(Regex.CLEAN_ADDRESS.toRegex(), " ")
    }
}
