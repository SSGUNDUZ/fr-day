package com.example.demo.utils

import java.util.regex.Matcher
import java.util.regex.Pattern

object AddressMatch {

    fun getAddressMatch(regex: String, address: String): Matcher {
        val pattern = Pattern.compile(regex)
        return pattern.matcher(address)
    }
}
