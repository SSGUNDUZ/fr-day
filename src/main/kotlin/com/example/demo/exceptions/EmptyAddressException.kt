package com.example.demo.exceptions

class EmptyAddressException : RuntimeException("Empty address cannot be interpreted")
