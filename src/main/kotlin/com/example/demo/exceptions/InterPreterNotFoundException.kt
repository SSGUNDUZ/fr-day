package com.example.demo.exceptions

class InterPreterNotFoundException(address: String) : RuntimeException(String.format("There is no interpreter for given address %s", address))
