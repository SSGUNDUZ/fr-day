package com.example.demo.controller

import com.example.demo.model.AddressDTO
import com.example.demo.service.AddressService
import org.slf4j.LoggerFactory
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RestController
import javax.validation.Valid

@RestController
class AddressController(private val addressService: AddressService) {

    private val logger = LoggerFactory.getLogger(AddressController::class.java)

    @PostMapping("/address")
    fun postAddress(@Valid @RequestBody fullAddress: String): AddressDTO {

        logger.info("Posting address starts with following address ${fullAddress}")

        return addressService.interpretAddress(fullAddress)
    }
}
