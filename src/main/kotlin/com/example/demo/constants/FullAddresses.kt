package com.example.demo.constants

class FullAddresses {

    companion object {
        const val AUF_DER_VOGELWIESE_ADDRESS = "Auf der Vogelwiese 23 b"
        const val BROADWAY_AV_ADDRESS = "200 Broadway Av"
        const val CALLE_39_ADDRESS = "Calle 39 No 1540"
        const val AM_BACHLE = "Am Bächle 23"
        const val BLAUFELWEG_123B = "Blaufeldweg 123B"
        const val RUE_DE_LA_REVOLUT = "4, rue de la revolution"
        const val CALLE_ADUANA = "Calle Aduana, 29"
    }
}
