package com.example.demo.constants

class Regex {

    companion object {
        val NUMBER_INCLUDING_ADDRESS = "(?= (?i)No|Number)"
        val SPECIAL_LETTERS_ALPHABET = "^([^0-9]+) ([\\w]+\\s?[a-zA-Z]?)"
        val LATINIC_ALPHABET = "^([\\w]+) ([^0-9]+)"
        val CLEAN_ADDRESS = ","

    }
}
