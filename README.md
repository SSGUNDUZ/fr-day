# Events Service

APIs to handle events.

## Pre-requisites
- Java 8
- Kotlin
- MongoDB 3.4+


## MongoDB Installation on local

- Refer - https://docs.mongodb.com/manual/tutorial/install-mongodb-on-os-x/

## Useful Commands:

```
# start the server
$ ./gradlew clean build
```
## POST Endpoint:

```
# server:port/address 
$ you can make a post call with text body
```
